import { motion } from "framer-motion";

const colorVariant = {
  initial: {
    color: "var(--color-text)",
  },
  final: {
    color: "var(--color-primary)",
  },
};

const opacityVariant = {
  initial: {
    opacity: 0,
  },
  final: {
    opacity: 1,
  },
};

const linkVariant = {
  initial: {
    opacity: 0,
    y: -10,
  },
  final: {
    opacity: 1,
    y: 0,
  },
};

const links = [
  {
    name: "mat",
    link: "https://mat.mathadvance.org",
  },
  {
    name: "mast",
    link: "https://mast.mathadvance.org",
  },
	{
		name: "mapm",
		link: "https://mapm.mathadvance.org",
	},
  {
    name: "cars",
    link: "https://cars.mathadvance.org",
  },
];

export default function Home() {
  return (
    <main className="px-8">
      <section className="flex flex-col items-center justify-center min-h-screen">
        <motion.h1
          initial="initial"
          animate="final"
          variants={opacityVariant}
          transition={{
            delay: 0.5,
            duration: 1,
            when: "beforeChildren",
          }}
          className="text-center text-6xl sm:text-7xl font-bold"
        >
          <motion.span variants={colorVariant} transition={{ duration: 0.5 }}>
            M
          </motion.span>
          ath{" "}
          <motion.span variants={colorVariant} transition={{ duration: 0.5 }}>
            A
          </motion.span>
          dvance
        </motion.h1>
        <motion.div
          initial="initial"
          animate="final"
          transition={{
            delayChildren: 2,
            staggerChildren: 0.5,
          }}
          className="mt-2 flex items-center gap-8 text-lg sm:text-2xl"
        >
          {links.map((link) => (
            <motion.div
              variants={linkVariant}
              transition={{ type: "spring", bounce: 0.6 }}
              key={link.link}
            >
              <a
                className="hover:text-primary duration-300 hover:duration-150 underline"
                href={link.link}
                target="_blank"
                rel="noreferrer"
              >
                {link.name}
              </a>
            </motion.div>
          ))}
        </motion.div>
      </section>
    </main>
  );
}
